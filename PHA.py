# For more information on all of the functions available use the NI-DAQmx C reference help page
# this python package (PyDAQmx) is a wrapper for those functions.
import PyDAQmx as nidaq
import numpy as np
import matplotlib.pyplot as plt
import datetime
from time import perf_counter
import sys

now = datetime.datetime.now()
current_date_time = str(now.strftime("%Y-%m-%d_%Hhrs%Mmins"))

timeWindow = input("Size of time window (default: 0.00001024 seconds): ")
while type(timeWindow) != float:
    try:
        if timeWindow == "":
            timeWindow = float(0.00001024)
        else:
            timeWindow = float(timeWindow)
    except:
        timeWindow = input("Error: Enter a valid time window in seconds (default: 0.00001024 seconds): ")

sampleFrequency = input("Sample frequency (default: 50000000 Hz): ")
while type(sampleFrequency) != float:
    try:
        if sampleFrequency == "":
            sampleFrequency = float(50000000)
        else:
            sampleFrequency = float(sampleFrequency)
    except:
        sampleFrequency = input("Error: Enter a valid sampleFrequency in Hz (default: 50000000 Hz): ")

minVoltage = input("Minimum Voltage (default: 0 V): ")
while type(minVoltage) != float:
    try:
        if minVoltage == "":
            minVoltage = float(0)
        else:
            minVoltage = float(minVoltage)
    except:
        minVoltage = input("Error: Enter a valid minimum voltage in V (default: 0 V): ")

maxVoltage = input("Maximum Voltage (default: 5 V): ")
while type(maxVoltage) != float:
    try:
        if maxVoltage == "":
            maxVoltage = float(5)
        else:
            maxVoltage = float(maxVoltage)
    except:
        maxVoltage = input("Error: Enter a valid maximum voltage in V (default: 5 V): ")

triggerType = input("Enter trigger type (rising or falling): ")
while triggerType not in ["rising", "falling"]:
    if triggerType == "":
        triggerType = "rising"
    else:
        triggerType = input("Error: please choose either (rising or falling): ")
if triggerType == "rising":
    triggerType = nidaq.DAQmx_Val_Rising
else:
    triggerType = nidaq.DAQmx_Val_Falling
# pretrigger Samples

triggerLevel = input("Enter trigger level (default: 1 V): ")
while type(triggerLevel) != float:
    try:
        if triggerLevel == "":
            triggerLevel = float(1)
        else:
            triggerLevel = float(triggerLevel)
    except:
        triggerLevel = input("Error: Enter a valid trigger threshold in V (default: 1 V):")

numSamples = int(timeWindow*sampleFrequency)
print(numSamples)
# num_samples = int(elapsed_time*sample_frequency)
time_tick = 1/sampleFrequency  # in seconds
time_axis = []      # in seconds
time = 0
while len(time_axis)<numSamples:
    time_axis.append(time)
    time += time_tick
time_axis = np.asarray(time_axis)

# This creates a task object to setup the digitizer to begin taking configurations
t = nidaq.Task()
# The arguments, in order: Path/to/channel/on/oscilloscope, An option to assign a name to the channel,
# configuration to set on the terminal used (Here we use Referenced single-ended mode), minimum voltage
# that we expect to measure, maximum voltage expected, units to use for data (Volts), finally scale to use
# on the data.
# The short of it: It creates a channel to measure Voltage and adds it to the Task object.
t.CreateAIVoltageChan("Dev1/0", None, nidaq.DAQmx_Val_RSE, minVoltage, maxVoltage, nidaq.DAQmx_Val_Volts, None)
# The arguments: Source terminal of the sample clock (we have it blank which set it to NULL and therefore
# uses the 'OnBoardClock'), Sampling rate in Samples/s, Active edge (Specifies on which edge of the clock 
# to acquire or generate samples.), sampleMode (Specifies whether the task acquires or generates samples 
# continuously or if it acquires or generates a finite number of samples. we use a finite amount),
# sampsPerChanToAcquire (number of samples to grab if you set it to 'finite')
t.CfgSampClkTiming("", sampleFrequency, nidaq.DAQmx_Val_Rising, nidaq.DAQmx_Val_FiniteSamps, numSamples)
# CfgAnlgEdgeRefTrig(self, triggerSource, triggerSlope, triggerLevel, pretriggerSamples)
# NOTE: Trigger Level has a minimum value of ~0.18 Volts
t.CfgAnlgEdgeRefTrig("Dev1/0", triggerType, np.float64(triggerLevel), 200)
# t.CfgAnlgEdgeStartTrig("Dev1/0", nidaq.DAQmx_Val_RisingSlope, 4)
# Testing to see if the next line sets the coupling to AC or DC NOTE: error message
# "Specified property is not supported by the device or is not applicable to the task."
# t.SetAnlgEdgeRefTrigCoupling(nidaq.DAQmx_Val_DC)


print("Starting Task")

def plotCumulative(frequencies, times):
    periods = []
    elapsedTimes = []
    for freq in frequencies:
        periods.append(1/float(freq))
    for t in times:
        elapsedTimes.append(float(t)/1000)
    plt.plot(periods,periods,'r-')
    plt.plot(periods,np.asarray(periods)*2,'y-')
    plt.plot(periods,elapsedTimes,'b.')
    plt.xlabel("period")
    plt.ylabel("elapsed time/1000")
    plt.xlim(0,0.05)
    plt.ylim(0,0.05)
    plt.show()

data = np.zeros((numSamples,), dtype=np.float64)
read = nidaq.int32()
allData = []
times = []
frequencies = []
i = 0

decidingToContinue = True
while decidingToContinue:
    decision = input("Do you already have the deadtime? (y or n): ")
    if decision not in ["y","n"]:
        decision = input("Do you already have the deadtime? (y or n): ")
    else:
        if decision == "y":
            determiningDeadtime = False
            deadTime = input("Input the deadtime in seconds: ")
            while type(deadTime) != float:
                try:
                    deadTime = float(deadTime)
                    decidingToContinue = False
                except:
                    deadTime = input("Input the deadtime in seconds: ")
        elif decision == "n":
            determiningDeadtime = True
            decidingToContinue = False

if determiningDeadtime:
    frequency = input("generator frequency (Hz): ")
    with open("elapsedTimeVsFrequency_PW2Microseconds_1000events_series6.txt", "w") as outFile:
        while determiningDeadtime:
            startTime = perf_counter()
            for i in range(1000):
                t.StartTask()

                t.ReadAnalogF64(numSamples, nidaq.DAQmx_Val_WaitInfinitely, nidaq.DAQmx_Val_GroupByChannel,
                data, len(data), nidaq.byref(read), None)
                allData.append(data)

                t.StopTask()
            endTime = perf_counter()
            elapsedTime = endTime - startTime
            frequencies.append(frequency)
            times.append(elapsedTime)
            outFile.write(f"{elapsedTime}\t{frequency}\n")
            plotCumulative(frequencies,times)
            response = "waiting"
            while response not in ['y', 'n']:
                response = input("Continue with a different frequency? (y or n): ")
                if response == 'n':
                    determiningDeadtime = False
                    deadTime = input("Input deadtime (s): ")
                    while type(deadTime) != float:
                        try:
                            deadTime = float(deadTime)
                        except:
                            seePlotAgain = input("Invalid deadtime. See plot again? (y or n): ")
                            if seePlotAgain == "y":
                                plotCumulative(frequencies, times)
                            deadTime = input("Input valid deadtime in seconds: ")
                elif response == 'y':
                    frequency = input("generator frequency (Hz): ")

def findHeight(pulse):
    pretriggerSampleRange = (10,190)
    baseline = np.average(pulse[pretriggerSampleRange[0]:pretriggerSampleRange[1]])
    baseline = 0
    pulseMax = np.max(pulse)
    pulseHeight = pulseMax - baseline
    return pulseHeight

pulses = []
cumulativePulseHeights = []
cumulativeLiveTime = 0
cumulativeRealTime = 0
numEvents = 100
generatingSpectrum = True
while generatingSpectrum:
    print("Begin generating Spectrum")
    startTime = perf_counter()
    for i in range(numEvents):
        t.StartTask()
        t.ReadAnalogF64(numSamples, nidaq.DAQmx_Val_WaitInfinitely, nidaq.DAQmx_Val_GroupByChannel,
        data, len(data), nidaq.byref(read), None)
        pulses.append(data)
        data = np.zeros((numSamples,), dtype=np.float64)
        # print(i)
        t.StopTask()
    endTime = perf_counter()
    realTime = endTime - startTime
    liveTime = realTime - deadTime*numEvents
    cumulativeRealTime += realTime
    cumulativeLiveTime += liveTime
    for pulse in pulses:
        height = findHeight(pulse)
        # plt.plot(pulse)
        # plt.show()
        cumulativePulseHeights.append(height)
    plt.hist(cumulativePulseHeights, bins=100)
    print("Cumulative Real Time:")
    print(cumulativeRealTime)
    plt.show()
    pulses = [] # or write them somewhere else
    decisionToContinue = input("Continue recording pulses? (y or n): ")
    while decisionToContinue not in ["y", "n"]:
        decisionToContinue = input("Continue recording pulses? (y or n): ")
    if decisionToContinue == "n":
        generatingSpectrum = False
fileName = input("Name the run: ")
with open(fileName+"-"+datetime.datetime.now().strftime("%H_%M_%S")+".dat", "w") as runsFile:
    runsFile.write("Real Time\tLive Time\n")
    runsFile.write(str(cumulativeRealTime)+"\t"+str(cumulativeLiveTime)+"\n")
    for h in cumulativePulseHeights:
        runsFile.write(str(h))
        runsFile.write("\n")

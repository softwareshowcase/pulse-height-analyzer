# Pulse Height Analyzer - National Instruments Digital Oscilloscope

## Running the analyzer

This program was designed to work with the National Instruments USB 5132 digital oscilloscope, but may work with other models that are similar. The text based interface allows you to configure parameters like the scope's range, resolution, and trigger threshold. You will need to figure out where the USB device is mounted (e.g. Dev1/0) to properly connect. Set it on the line containing:
t.CfgAnlgEdgeRefTrig("Dev1/0", triggerType, np.float64(triggerLevel), 200)
Currently the program is set to run until a set number of events are recorded. This can be slightly modified to run for a certain real time, but may not behave as expected if the events are sparse.

## Deadtime

For statistical purposes, you may want the deadtime on the measurements you make with the program. The program asks if you already have the deadtime on your system. If you don't, the program allows you to send test pulses at periods that are comparable to the system deadtime (This depends not only on the digital oscilloscope, but also the state of the computer running the program). Results can be plotted as you take each measurement for tuning purposes.

## Potential Deadtime Improvement

Some NI USB digital oscilloscopes support hardware trigger resets that can potentially decrease deadtime that would otherwise be eaten up by the software trigger reset.
